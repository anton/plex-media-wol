# plex-media-wol

This script will watch for incoming connections to your Plex Media Server and automatically send a magic packet to a remote host. This is especially useful if the remote host contains media shares for Plex and the remote host might be sleeping.

## Prerequisites

- `wakeonlan` - installed via `sudo apt install wakeonlan`

## To install

1. Clone this repository to `/opt/plex-media-wol`
2. In order for the script to work, we need to log all `NEW` connections to the inbound Plex port (by default, this is `32400`):

    ```sh
    iptables -I FORWARD -i <INTERFACE> -p tcp --dport <PLEX_PORT> -m state --state NEW -j LOG --log-prefix "PLEX Connection "
    ```
3.  To ensure we don't get any false positives when the Plex app servers contact Plex Media Server, we need to download an up-to-date list of Plex app IP addresses:

    ```sh
    */10 * * * * curl -s https://s3-eu-west-1.amazonaws.com/plex-sidekiq-servers-list/sidekiqIPs.txt > /opt/plex-media-wol/plex_ips.txt
    ```
4. Install the `plex-media-wol.service`:

    ```sh
    ln -s /opt/plex-media-wol/plex-media-wol.service /lib/systemd/system/plex-media-wol.service
    systemctl daemon-reload
    systemctl enable plex-media-wol.service
    ```
5. Add your broadcast address and MAC address for the remote host to wake:

    ```sh
    mkdir /etc/systemd/system/plex-media-wol.service.d
    editor /etc/systemd/system/plex-media-wol.service.d/envs.conf
    ```
    ```
    [Service]
    Environment="PMW_BROADCAST_ADDR=x.x.x.x"
    Environment="PMW_MAC_ADDR=xx:xx:xx:xx:xx:xx"
    Environment="PMW_IP_ADDR=x.x.x.x"
    Environment="PMW_PORT=x"
    ```
6. Start the `plex-media-wol.service`

    ```sh
    systemctl start plex-media-wol.service
    ```

## Logs

The logs can be watched by running `journalctl`:

```sh
journalctl -f -u plex-media-wol.service
```

The raw logs from `iptables` can be watched as well:

```sh
tail -f /var/log/kern.log | grep "PLEX Connection"
```
