#!/bin/bash

TIMEOUT=60
WOL_AFTER=$(expr $EPOCHSECONDS - 1)

wol() {
    # Only send a magic packet if we aren't in the timeout period
    if [ $WOL_AFTER -lt $EPOCHSECONDS ]; then

        # Check if the host is online
        nc -zv $PMW_IP_ADDR $PMW_PORT -w 1 2>/dev/null
        if [[ "$?" -eq 0 ]]; then
            echo "Host is currently online, skipping WOL"
        else
            echo $(wakeonlan -i $PMW_BROADCAST_ADDR $PMW_MAC_ADDR)
        fi

        WOL_AFTER=$(expr $TIMEOUT + $EPOCHSECONDS)
    else
        echo "Sleeping for $(expr $WOL_AFTER - $EPOCHSECONDS)s, skipping WOL"
    fi
}

while read LINE; do

    # Skip any lines that aren't Plex connection related
    if [[ $LINE != *"PLEX Connection"* ]]; then
        continue
    fi

    # Get the source IP address
    SRC=$(echo $LINE | grep -oP "(?<=SRC=)[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}")

    # If the IP address is from the external Plex app, skip it
    if grep -q $SRC /opt/plex-media-wol/plex_ips.txt; then
        echo "Plex app IP detected $SRC, skipping WOL"
        continue
    fi

    # Skip any requests from localhost
    if [[ $SRC == "127.0.0.1" ]]; then
        continue
    fi

    # Send a WOL magic packet
    wol $SRC

done < <(tail -n1 -f /var/log/kern.log)
